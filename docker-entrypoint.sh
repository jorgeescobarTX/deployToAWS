#!/bin/bash

set -e

# The file structure is: hostname:port:database:username:password
echo "$POSTGRES_HOST:5432:$POSTGRES_DB:$POSTGRES_USER:$POSTGRES_PASSWORD" > ~/.pgpass

chmod 600 ~/.pgpass

printenv | grep -v "no_proxy" >> /etc/environment

exec "$@"
