#!/bin/bash

# If an error is found then the process is stopped and 
# a error number returned
set -e

# Getting the current date time
echo 'Generating the filename' >> /var/log/cron.log
now=$(date +'%Y%m%d%H%M')

# To generate the backup filename in some specific format
# i.e. processor-201710311256
# The $POSTGRES_DB is according to the environment variables defined
filename="$POSTGRES_DB-$now"

echo 'Filename generated ' $filename >> /var/log/cron.log

# Getting the backup using pg_dump and storing it
# using the filename generated previously
echo 'Generating the backup' >> /var/log/cron.log
pg_dump -h $POSTGRES_HOST -Fc -x --no-owner -U $POSTGRES_USER $POSTGRES_DB > /tmp/$filename

echo 'Pushing the backup' >> /var/log/cron.log

S3_KEY=$S3_BUCKET/backups/$filename-backup.gz
echo 'S3_KEY ' $S3_KEY >> /var/log/cron.log

aws s3 cp /tmp/$filename s3://$S3_KEY --sse AES256

echo 'Removing the backup from the container' >> /var/log/cron.log
rm /tmp/$filename

