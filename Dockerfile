FROM postgres:9.6

# Install the pre-reqs and aws-cli
RUN apt-get update && apt-get install -y python && apt-get install -y python-pip && pip install awscli

# Copy the script that generate the backups in the container root
COPY backup.sh /

# Copy the cron job
COPY cron-backup /etc/cron.d/

RUN chmod 0644 /etc/cron.d/cron-backup

# Create the log file for the cron
RUN touch /var/log/cron.log

# Copy the configuration script
COPY docker-entrypoint.sh /

# Using the configuration script
ENTRYPOINT ["/docker-entrypoint.sh"]

# The cron command is executed to run the cron job in the cron-backup
# The tail command help us to keep alive the container and dont exists after cron execution
CMD cron && tail -f /var/log/cron.log
